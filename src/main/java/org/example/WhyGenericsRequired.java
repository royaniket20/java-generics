package org.example;

import java.util.ArrayList;

public class WhyGenericsRequired {
    public static void main(String[] args) {
        System.out.println("Hello world Aniket !");

        //What happens when Generics not available
        ArrayList names = new ArrayList();
        //Java do not know what type of data type you will Store - So they assume it will be Object -
        //As its super class of Everything - Because superclass reference can be used to hold subclass Obj
        names.add("Name 1");
        names.add("Name 2");
        names.add(true); //Compile will never Complaint
        names.add(11);  //Compile will never Complaint

        //This will Not work - as because add(..) Type of the Argument is Object
        //Same for get(x) ... Type of Return Type is also Object
       // String firstName =names.get(0);

        //you don't also know suppot what is the data type, actually.
        String firstName =(String) names.get(0); //I need to manually cast it but its painful to do cast
        System.out.println("----Output -----"+firstName);

        try{
            String badName =(String) names.get(3); //I need to manually cast it but its painful to do cast
            System.out.println("----Output -----"+badName);
        }catch (Exception ex){
            //It will throw Class cast Exception - Now that's bad
            System.out.println("Exception happened --> "+ ex.getMessage());
        }

        //So to prevent all these Situation you should use Generics
        ArrayList<String> namesData = new ArrayList<String>();
        namesData.add("Name 1");
        namesData.add("Name 2");
        //namesData.add(true); //Compile will Now Complaint
       // namesData.add(11);  //Compile will Now Complaint

        String firstNameGood = namesData.get(0); //No need to manually Cast during retrieval
        System.out.println("----Output -----"+firstNameGood);

        //Generics empower compiler to ensure of Strict Type is allowed - But Only for Non Primitive Types
        //ArrayList<int> invalidData = new ArrayList<int>(); - So when Generics Come - Autoboxing is also introduced to use Wrapper classes




    }
}