package org.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ParameterizedTypes {
    public static void main(String[] args) {
        List<String> data = new ArrayList<>();
        //data =It remember the parameterized type it associated with ,Whenever we will use this variable
        //it can understand the associative type.
        //So when we call [data.iterator()] ==It knows it need to Give Iterator<String>
        data.add("One");
        data.add("two");

        Iterator<String> stringIterator = data.iterator(); //Here Iterator is of type String
        //This is because List is of Type String
        //So the Generic Type Flow from one level to another as designed.

        stringIterator.forEachRemaining(item->{
            System.out.println("Result ::"+ item);
        });
        /**
         * List<param> == param -> Its a Java Type you are passing in between <> == So that compile can safeguard the Structure
         * public interface List<E>  ===> List is a Generic Type because it has <E> - and then It assume of the Type E
         * however if ypu do not pass the Type - It will assume Object Type
         *
         * public interface List<E>  ====> List<String> data
         *  boolean add(E e);        =====>  add(String e)
         *   E get(int index);    =======>  String get(int index)
         *
         *    Iterator<E> iterator(); ===> Here the Method return a Generic Type -that is also possible
         */
    }
}
