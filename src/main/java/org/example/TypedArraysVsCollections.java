package org.example;

import java.util.ArrayList;
import java.util.List;

public class TypedArraysVsCollections {
    /**
     * Arrays are Run Time construct
     */
    public static void main(String[] args) {

        List<String> namesList = new ArrayList<>();
        addNames("Name 1" , namesList);
        addNames("Name 2" , namesList);
        addNamesBad(1122, namesList);
        System.out.println("Result : "+ namesList);

        String[] nameArr = new String[5];//Typed Array
        addNamesArr("Aniket",nameArr);
        System.out.println("*******Result --------");
        String name11 = nameArr[0];
        System.out.println("This will work --"+name11);
        addNamesBadArr(1122,nameArr); //Code will break here at Runtime
        //When we are putting the value to Array that time directly Program break
        //Although Compile allowed it to compile by fooling it
        //But Runtime know that Still this instance is of String Array - So At Runtime Still JVM knows its a String Array
        //So in Generics case it failed - because It do not know the type when you are adding Value to List
        //But when yopu retrived the data and tried to use it as String it breaks
        //If you had used Object - this Could have worked
        // String result = names.get(2); ===> Object result = names.get(2); [Will work perfectly]
        String name22 = nameArr[1];
        System.out.println("Code will never reach here ---"+ name22);
    }


    private static void addNamesArr(String input, String[] names) {
        names[0] = input;
    }
    private static void addNamesBadArr(Integer input, Object[] names) {
        names[1] = input;
    }

    private static void addNames(String input, List<String> names) {
        names.add(input);
    }
    private static void addNamesBad(Integer input, List names) {
        names.add(input);
    }
}
