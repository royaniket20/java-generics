package org.example;

import java.util.ArrayList;
import java.util.List;

public class GenericsInheritanceLimiting {
    //Inheritance in Generics
    /**
     * In Generics Extension works in complete different way
     */

    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("Aniket");
        names.add("Sriparna");
        List<Integer> nums = new ArrayList<>();
        nums.add(11);
        nums.add(22);
        List<Double> dubs = new ArrayList<>();
        dubs.add(11.5678);
        dubs.add(22.54323);
        List<Float> flts = new ArrayList<>();
        flts.add(11.23f);
        flts.add(22.22f);
        printListGeneral(names);
        printListGeneral(nums);
        printListGeneral(dubs);
        printListGeneral(flts);

        //printListLimited(names); //Will Not work
        printListLimited(nums);
        printListLimited(dubs);
        printListLimited(flts);
       List<Number> numberList = new ArrayList<>();
        printListLimitedLower(numberList);
    }

    private static void printListGeneral(List<?> names) {
        System.out.println("****** printListGeneral ********");
        names.forEach(System.out::println);
    }

    /**
     *  This is a Upper Bound Wildcard - you can match Same or Subclass of It
     * @param names
     */
    private static void printListLimited(List<? extends  Number> names) {
        System.out.println("****** printListLimited ********");
        names.forEach(System.out::println);
        Number number = names.get(0);//Allowed
        /**
         * Compile Still dont know what Kind of Numer it is
         */
        //Integer integer = names.get(0); //NoT allowed
        //names.add(11.34);//Will also Not work
        Number number1 = 11;
        //names.add(number1);//Even This will also Not work

    }
    /**
     * Lower Bound Wildcard Generics - This will allow you writing to List - wHICH IS lIMITED FOR Upper Bound Wildcard
     * Generic has to be atleast of Numer or Super class Of it
     * PECS = Producer Extends Consumer Super
     * When generics Giving you data - you are printing - use extends [Upper Bound]
     * When generics is Capturing , You are producing - Use super[Lower Bound]
     */
    private static void printListLimitedLower(List<? super   Number> names) {
        System.out.println("****** printListLimited ********");

        //Now I want to add some value
        Number number = 11;
        names.add(number);
        names.add(22.45);
        names.add(345.56f);
        names.add(34);
        names.forEach(System.out::println);
    }
}
