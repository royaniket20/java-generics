package org.example;

import java.util.ArrayList;
import java.util.List;

public class GenericsInheritanceWildCard {
    //Inheritance in Generics
    /**
     * In Generics Extension works in complete different way
     */

    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("Aniket");
        names.add("Sriparna");
        printList(names);
        printListGeneral(names);
        List<Integer> nums = new ArrayList<>();
        nums.add(11);
        nums.add(22);
        //This will not work If we use the  private static void printList(List<String> names)
        //printList(nums);
        /**
         * How to create a General Purpose printer
         * We could have do - private static void printList(List<Object> names) - But it will also not work
         * Why it's not working - because in Genrics do not handle Inheritance in this way although Both String , Integer is having Parent Object
         *Generics is stricter to exact match - This is now allowed
         * Because if we allow this - then A list could have contains Multiple data types
         * Which will break Generics Type safety - So we need to use Generics Wildcard
         * PLEASE DONT USE  private static void printList(List names) //RAW TYPE -BECAUSE TYPE ERASER PROBLEM
         */
        printListGeneral(nums);

        List<?> tmp = names; //Will work
        tmp = nums; //Will work
        tmp.get(0);//Will work
        /**
         * Compiler do not know code flow and not sure what type is actually assigned to it
         */
        //tmp.add(11);//Will not work as Compiler is Not sure what is the type it shoud hold
    }

    private static void printList(List<String> names) {
        names.forEach(System.out::println);
    }


    /**
     *
     * @param names
     * List<?> == List of whatever
     * So when someone is calling the Method then Type get assigned
     */
    private static void printListGeneral(List<?> names) {
        /**
         * When ? is the type - So compiler do not know what is the type - it will not allow
         * you to add any element to It
         * But at least you can read data from it
         */
        //names.add("Aniket"); //Will not work
        //names.add(11); //Will Not work
        //String name = names.get(0); --> This will not be allowed also
        Object data = names.get(0); //This will work - As no type casting happening
        System.out.println("Data read - "+data);
        names.forEach(System.out::println);
    }
}
