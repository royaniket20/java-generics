package org.example;

import java.util.ArrayList;
import java.util.List;

public class MixGenericAndNonGenerics {
    /**
     * We know we should use Generic Types only together - Not with Generic Non type - But we can use it
     * @param args
     */
    public static void main(String[] args) {
        //Generics is a fundamental property of Compile time
        //Generics is a Type eraser
        //Generics checking all Done at Compile time.Bytecode do not have any Generic Information
        //So once bytecode is done --List<String> ==== List<Integer> {All are same}

        //Generics Runtime Check
        List<String> names = new ArrayList<>();
        addNames("Name 1" , names);
        addNames("Name 2" , names);

        List names2 = names; //Here we are Assigning Generic type to Non Generic Type
        System.out.println("Result : "+ names2);
        addNamesBad(1122,names);
        System.out.println("Result : "+ names);

        /**
         * Result : [Name 1, Name 2]
         * Result : [Name 1, Name 2, 1122]
         * Although there is Generics Protection on names , But if we mix it with Non Generic Type - It will allow at Runtime
         * To add incompatible types and eventually will break Code
         */
        //Fooling the Compiler
        try {
            String result = names.get(2); //Compiler think its A string type
            //This proofs that at Runtime Instance of the List do not have Type reference
            //All happens at Compile time and prevent Developer from doing silly things
            //So Generics type is a Compile type Construct - Once bytecode get created - all these type reference is ERASED -TYPE ERASED
        }catch (Exception ex){
            System.out.println("Error - "+ ex);
        }
    }

    private static void addNames(String input, List<String> names) {
        names.add(input);
    }
    /**
     * Java Generics is fully Backward comparable - can work along with Non Generic
     */


    private static void addNamesBad(Integer input, List names) {
        names.add(input);
    }

}
