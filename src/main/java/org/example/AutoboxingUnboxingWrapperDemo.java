package org.example;

import java.util.ArrayList;
import java.util.Arrays;

public class AutoboxingUnboxingWrapperDemo {

    public static void main(String[] args) {
        /**
         * Autoboxing : this actually create a new Instance of Wrapper class , and then assign Value TO Obj attribute.
         *
         * Unboxing : This will convert a Wrapper Class Obj to Primitive value
         *
         *
         */

        int i = 10; //Primitive type
        int x = i;
        //From Java 5 Onwards Wrapper Object is introduced - Because Primitives are Not Object
        //So Wrapper Object wrap a Primitive to make it a Object - which can be handled in Java Way
        Integer integer = i; //Autoboxing
        System.out.println("Wrapper Value : "+ integer);
        int val = integer; //Unboxing
        System.out.println("Primitive Value : "+ val);

        /**
         * Autoboxing , Unboxing will be super helpful in case of Generics
         */
        ArrayList<Integer> integers = new ArrayList<Integer>();
        integers.add(11);
        int data = 99;
        integers.add(data); //Autoboxing happening
        integers.addAll(Arrays.asList(11,22,data,i));
        int result = integers.get(1); //Unboxing Happens
        System.out.println("Values ====>"+integers);
    }
}
