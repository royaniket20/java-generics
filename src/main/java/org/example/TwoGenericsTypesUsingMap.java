package org.example;

import java.util.HashMap;
import java.util.Map;

public class TwoGenericsTypesUsingMap {

    public static void main(String[] args) {
        //Raw Map type - No Generics Given
        Map map = new HashMap();
        map.put("11",11);
        map.put(11,"11");
        //Here we are giving Two Generics Types - One for Key , One for Value
        //public interface Map<K, V> - Letters denoting Generics Do not matter
        Map<String,Integer> integerMap = new HashMap<>();
        integerMap.put("11",11);
        //integerMap.put(11,"11"); //Will not work
    }
}
