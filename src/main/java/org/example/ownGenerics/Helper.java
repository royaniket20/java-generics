package org.example.ownGenerics;

public class Helper {

    public static void main(String[] args) {
        OldCage oldCage = new OldCage();
        oldCage.setAnimal1(new Monkey());
        oldCage.setAnimal2(new Lion()); //This is Bad  === Because Lion will attack Monkey
        System.out.println(oldCage);
        //We need to prevent from happening
        //We should keep Monkey only in MonkeyCage
        Cage<Monkey> monkeyCage = new Cage<Monkey>();
        monkeyCage.setAnimal1(new Monkey());
        //monkeyCage.setAnimal2(new Lion());//Will Give compilation Error
        monkeyCage.setAnimal2(new Monkey());

        System.out.println(monkeyCage);
        /**
         * Tomorrow Someone can create N type of Animal , Now then They can easily create Cage of that type
         * and then use it for Storing those Animals
         * Its will always be Generic in Nature
         */


        //Now use Generics in Constructor

        Cage<Monkey> newCage = new Cage<Monkey>(new Monkey(), new Monkey());
        System.out.println("New Result ----> " + newCage);

        Cage<Monkey> newCageBad = new Cage(new Monkey(), new Lion()); //This is Bad
        //Why this is happening because Constructor is Not of Generic Type - So Constructor get resolved and
        //Then assigning it to Cage Type - So fooling Compiler
        //You should use it
        // Cage<Monkey> newCageBad = new Cage<Monkey>(new Monkey(),new Lion());  --This will fail - Because Now Constructor expression is also Generic Type
        System.out.println("New Result ----> " + newCageBad);

        /**
         * So when you have Arguments better you use Generic Typed Constructor
         */



    }
}
