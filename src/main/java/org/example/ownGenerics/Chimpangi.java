package org.example.ownGenerics;

public class Chimpangi {
    String name = "Chimpangi";

    @Override
    public String toString() {
        return "Chimpangi{" +
                "name='" + name + '\'' +
                '}';
    }
}
