package org.example.ownGenerics;

public class Lion extends  Animal implements Eats,Sleeps {
    String name = "Lion";

    @Override
    public String toString() {
        return "Lion{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void eat() {
        System.out.println("I am eating Lion");
    }

    @Override
    public void sleep() {
        System.out.println("I am sleeping Lion");
    }
}
