package org.example.ownGenerics;

public class Monkey extends  Animal implements  Sleeps,Eats {
    String name = "Monkey";

    @Override
    public String toString() {
        return "Monkey{" +
                "name='" + name + '\'' +
                '}';
    }


    @Override
    public void sleep() {
        System.out.println("I am sleeping monkey");
    }

    @Override
    public void eat() {
        System.out.println("I am eating monkey");
    }
}
