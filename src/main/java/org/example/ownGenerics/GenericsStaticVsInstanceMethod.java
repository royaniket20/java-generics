package org.example.ownGenerics;

public class GenericsStaticVsInstanceMethod {

    public static void main(String[] args) {
        /**
         * Suppose you want to Introduce a Static method in Generic Class
         * When Static method is used - You need to Pass Type as part of method
         */
        Monkey monkey1 = new Monkey();
        Lion lion = new Lion();
        Monkey monkey2 = new Monkey();
        boolean compatible = CageStaticMethod.isCompatiable(monkey2, monkey1);
        System.out.println("Is Compatiable ----"+ compatible);

        compatible = CageStaticMethod.isCompatiable(monkey2, lion);
        System.out.println("Is Compatiable ----"+ compatible);
    }
}
