package org.example.ownGenerics;

//I want to Store any kind of Animal
//Animals do not share any Common Interface
//Else we could have use Animal animal1 , animal2;

/**
 * Other Option Could be Object animal1 , animal2;
 *
 * @param <E>
 */

//Here we can Put even Human Also
//public class Cage<E> {
//Here we can Put Only Animals
//public class Cage<E extends  Animal> {

//Oder matter - First Class , Then Interface 1,2,3 - Suppose Lion only
// Implements Eats not Sleep - then Lion cannot be put in this Cage - As its Strictly Bound
//Generics can have Only one class and N number of Interfaces || All Interfaces
public class CageStaticMethod<E extends Animal & Sleeps & Eats> {
    private E animal1;
    private E animal2;

    public CageStaticMethod() {
    }

    public CageStaticMethod(E animal1, E animal2) {
        this.animal1 = animal1;
        this.animal2 = animal2;
    }

    public E getAnimal1() {
        return animal1;
    }

    public void setAnimal1(E animal1) {
        this.animal1 = animal1;
    }

    public E getAnimal2() {
        return animal2;
    }

    public void setAnimal2(E animal2) {
        this.animal2 = animal2;
    }

    @Override
    public String toString() {
        return "Cage{" +
                "animal1=" + animal1 +
                ", animal2=" + animal2 +
                '}';
    }


    public void feedAnimal() {
        //animal1.eat(); //Will not work - As Cage do not know that Animal's Children implemented those Interface
        animal1.eat();
        animal2.sleep();
    }

    //---E cannot be referenced from Static method - as No Instance get created - So How we will know the Type before that
    //So we need to give Generics Type in Method Signature
    public static  <E extends  Animal> boolean isCompatiable(E animal1 , E animal2 ){
       return animal1.getClass().getTypeName().equals( animal2.getClass().getTypeName());
    }
}
