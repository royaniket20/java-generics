package org.example.ownGenerics;

public class OldCage {
    private Object animal1;
    private Object animal2;

    public Object getAnimal1() {
        return animal1;
    }

    public void setAnimal1(Object animal1) {
        this.animal1 = animal1;
    }

    public Object getAnimal2() {
        return animal2;
    }

    public void setAnimal2(Object animal2) {
        this.animal2 = animal2;
    }

    @Override
    public String toString() {
        return "OldCage{" +
                "animal1=" + animal1 +
                ", animal2=" + animal2 +
                '}';
    }
}

